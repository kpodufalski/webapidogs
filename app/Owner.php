<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    protected $fillable = [
        'name',
        'img',
    ];


    public function dogs() {
        return $this->hasMany('App\Dog');
    }
}
