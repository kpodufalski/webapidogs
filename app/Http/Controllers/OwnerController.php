<?php

namespace App\Http\Controllers;

use App\Dog;
use Illuminate\Http\Request;
use App\Owner;

class OwnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $fillable = [
        'name',
        'url'
    ];

    public function index(Request $req)
    {
        $query = Owner::query();

        $orderProperty = $req->input('order', 'name');
        $direction = $req->input('direction', 'asc');

        $query->orderBy($orderProperty, $direction);

        return $query->get();
    }

    public function store(Request $req)
    {

        $params = $req->only('name', 'img', 'owner_id');
        return Owner::create($params);


    }

    public function show(Request $req, Owner $Owner)
    {
        return $Owner;
    }

    public function update(Request $request, $Owner) {
        $Owners = Owner::findOrFail($Owner);
        $Owners->update($request->all());

        return $Owners;
    }

    public function destroy(Owner $Owner)
    {
        $Owner->delete();
        return $Owner;
    }

    public function dogs(Request $req){



    }
}

