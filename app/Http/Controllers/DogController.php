<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dog;
use App\Owner;

class DogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $fillable = [
        'name',
        'url',
        'owner_id'
    ];

    public function index()
    {
        $dogs = Owner::has('dogs')->get();
        return $dogs;
    }

    public function store(Request $req)
    {

        $params = $req->only('name', 'url', 'img', 'owner_id');
        return Dog::create($params);
    }

    public function show(Request $req, Dog $dog)
    {
        return $dog;
    }

    public function update(Request $request, $dog) {
        $dogs = Dog::findOrFail($dog);
        $dogs->update($request->all());

        return $dogs;
    }

    public function destroy(Dog $dog)
    {
        $dog->delete();
        return $dog;
    }
}
