<?php

use Illuminate\Database\Seeder;
use App\Dog;

class DogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        // And now, let's create a few articles in our database:
        for ($i = 0; $i < 50; $i++) {
            Dog::create([
                'name' => $faker->sentence,
                'img' => $faker->imageUrl(),
                'url' => $faker->url
            ]);
        }
//        $faker = \Faker\Factory::create();
//        for ($i = 0; $i < 0; $i++) {
//            DogController::create([
//                'name' => $faker->name,
//               'url' => $faker->url,
//                'img' => $faker->imageUrl()
//            ]);
//        }
//        DogController::create([
//            'name' => 'Bo',
//            'img' => 'https://i.amz.mshcdn.com/9xDFv-klq_nrHPBvVRZ3DCzL_oE=/950x534/2015%2F10%2F09%2Fc9%2Fboobama.29ff8.jpg',
//            'url' => 'https://en.wikipedia.org/wiki/Bo_(dog)'
//        ]);
    }
}
