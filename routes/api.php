<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// Create / store
Route::post('dogs', 'DogController@store')->name('dogs.store');

// Read all
//Route::get('dogs', 'DogController@index')->name('dogs.index');

// Read specific dog
Route::get('dogs/{dog}', 'DogController@show')->name('dogs.show');


Route::put('dogs/{dog}', 'DogController@update')->name('dogs.update');

// Delete
Route::delete('dogs/{dog}', 'DogController@destroy')->name('dogs.destroy');


// Create / store
Route::post('owners', 'OwnerController@store')->name('owner.store');

// Read all
Route::get('owners', 'OwnerController@index')->name('owner.index');

// Read specific dog
Route::get('owners/{owners}', 'OwnerController@show')->name('owner.show');


Route::put('owners/{owners}', 'OwnerController@update')->name('owner.update');

// Delete
Route::delete('owners/{owners}', 'OwnerController@destroy')->name('owner.destroy');


Route::get('owner/{dog}/dogs', 'DogController@index')->name('owner.dogs.index');
